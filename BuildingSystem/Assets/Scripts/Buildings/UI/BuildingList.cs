﻿using System.Collections.Generic;
using UnityEngine;

namespace Buildings
{
    public class BuildingList : MonoBehaviour, IBuildingList
    {
        public GameObject Handler => this.handler;
        public IBuildingView Prefab => this.prefab;
        IEnumerable<IBuilding> IBuildingList.List => this.list;

        [SerializeField] protected GameObject handler;
        [SerializeField] protected BuildingView prefab;
        [SerializeField] protected List<Building> list;

        private void Awake()
        {
            if(this.handler == null)
            {
                Debug.LogWarning("A warrning assigned to building list handler!", this);
            }

            if(this.prefab == null)
            {
                Debug.LogWarning("A warrning assigned to building list prefab!", this);
            }
        }

        public void Create()
        {
            foreach (var element in this.list)
            {
                BuildingView listElement = Instantiate(this.prefab, this.handler.transform.position,
                                                     this.handler.transform.rotation, this.handler.transform);
                
                listElement.Building = (IBuildingType)element;
                listElement.Initialize();
            }
        }
    }
}