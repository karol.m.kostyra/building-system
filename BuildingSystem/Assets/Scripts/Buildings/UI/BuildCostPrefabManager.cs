﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Buildings
{
    public class BuildCostPrefabManager : MonoBehaviour, IBuildCostPrefabManager
    {
        public Text ResourceName => this.resourceName;
        public Image ResourceImage => this.resourceImage;
        
        [SerializeField] protected Text resourceName;
        [SerializeField] protected Image resourceImage;

        public void SetResourceName(string name) => this.resourceName.text = name;

        public void SetResourceImage(Sprite sprite) => this.resourceImage.sprite = sprite;
    }
}