﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Buildings
{
    public interface IBuildingList
    {
        GameObject Handler { get; }
        IBuildingView Prefab { get; }
        IEnumerable<IBuilding> List { get; }
        void Create();
    }
}