﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Resources;

namespace Buildings
{
    public class Building : ScriptableObject, IBuilding
    {
        public string Name => this.name;
        public string Description => this.description;

        public int CurrentLevel
        {
            get => this.currentLevel;
            set
            {
                if (this.currentLevel == value) return;
                this.currentLevel = value;
                this.OnValueChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        public int MaxLevel
        {
            get => this.maxLevel;
            set
            {
                if (this.maxLevel == value) return;
                this.maxLevel = value;
                this.OnValueChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        public float BuildTime
        {
            get => this.buildTime;
            set
            {
                if (this.buildTime == value) return;
                this.buildTime = value;
                this.OnValueChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        public IResourcesManager ResourcesManager => this.resourcesManager;
        public event EventHandler<EventArgs> OnValueChanged;

        [SerializeField] protected new string name;
        [SerializeField] protected string description;
        [SerializeField] protected int currentLevel;
        [SerializeField] protected int maxLevel;
        [SerializeField] protected float buildTime;
        [SerializeField] protected ResourcesManager resourcesManager;
    }
}