﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Buildings
{
    [CreateAssetMenu(fileName = "Storage", menuName = "Statek/Buildings/Storage")]
    public class StorageBuilding : DefaultBuildingType, IStorageBuilding
    {
        public int Capacity => this.capacity;
        public int CapacityPerLevel => this.capacityPerLevel;
        
        [SerializeField] protected int capacity;
        [SerializeField] protected int capacityPerLevel;
    }
}