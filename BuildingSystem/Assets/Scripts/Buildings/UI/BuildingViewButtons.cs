﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Buildings
{
    [Serializable]
    public class BuildingViewButtons : IBuildingViewButtons
    {
        public Button BuildButton => this.buildButton;
        public Button UpgradeButton => this.upgradeButton;

        [SerializeField] protected Button buildButton;
        [SerializeField] protected Button upgradeButton;


        public void SetupBuildButton(DefaultBuildingType building)
        {
            this.buildButton.onClick.AddListener(building.Build);
        }

        public void SetupUpgradeButton(DefaultBuildingType building)
        {
            this.upgradeButton.onClick.AddListener(building.Upgrade);
        }
    }
}