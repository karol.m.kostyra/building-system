﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Resources
{
    public interface IResourcesManager
    {
        IEnumerable<IResource> Resources { get; set; }
        bool IsEnoughResources(IEnumerable<IResource> resources);
    }
}