﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Resources
{
    [Serializable]
    public class ResourcesManager : IResourcesManager
    {
        public IEnumerable<IResource> Resources { get => this.resources; set => this.resources = (List<Resource>)value; }
        
        [SerializeField] protected List<Resource> resources;

        public bool IsEnoughResources(IEnumerable<IResource> resources)
        {
            //TO_DO
            return true;
        }
    }
}