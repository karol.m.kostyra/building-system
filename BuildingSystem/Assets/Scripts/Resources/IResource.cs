﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Resources
{
    public interface IResource
    {
        IResourceData ResourceData { get; }
        int Amount { get; }
    }
}