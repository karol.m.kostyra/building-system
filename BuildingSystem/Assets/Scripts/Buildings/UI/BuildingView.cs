﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Resources;

namespace Buildings
{
    public class BuildingView : MonoBehaviour, IBuildingView
    {
        public IBuildingType Building { get => this.building; set => this.building = (DefaultBuildingType)value; }
        public IBuildingViewButtons BuildingViewButtons { get => this.buttons; }
        public Text Name => this.nameText;
        public Text Description => this.descriptionText;
        public Text Level => this.levelText;
        public Text BuildTime => this.buildTimeText;
        public GameObject BuildCostHandler => this.buildCostHandler;
        public GameObject BuildCostPrefab => this.buildCostPrefab;
        
        [SerializeField] protected DefaultBuildingType building;
        [SerializeField] protected BuildingViewButtons buttons;
        [SerializeField] protected Text nameText;
        [SerializeField] protected Text descriptionText;
        [SerializeField] protected Text levelText;
        [SerializeField] protected Text buildTimeText;
        [SerializeField] protected GameObject buildCostHandler;
        [SerializeField] protected GameObject buildCostPrefab;

        public void Initialize()
        {            
            UpdateData(this, new EventArgs { });
            this.building.OnValueChanged += UpdateData;
            this.buttons.SetupBuildButton(this.building);
            this.buttons.SetupUpgradeButton(this.building);
        }

        public void DisplayName()
        {
            this.nameText.text = this.building.Name;
        }

        public void DisplayDescription()
        {
            this.descriptionText.text = this.building.Description;
        }

        public void DisplayLevel()
        {
            this.levelText.text = this.building.CurrentLevel.ToString() + " / " + this.building.MaxLevel.ToString();
        }

        public void DisplayBuildTime()
        {
            this.buildTimeText.text = this.building.BuildTime.ToString();
        }

        public void DisplayBuildCost()
        {
            List<IResource> resourceList = this.building.ResourcesManager.Resources.ToList();

            foreach (var element in resourceList)
            {
                GameObject resource = Instantiate(this.buildCostPrefab, this.buildCostHandler.transform);

                var prefabManager = resource.GetComponent<BuildCostPrefabManager>();
                prefabManager.SetResourceName(element.ResourceData.Name + ": " + element.Amount.ToString());
                prefabManager.SetResourceImage(element.ResourceData.Sprite);
            }
        }

        public void UpdateData(object sender, EventArgs e)
        {
            DisplayName();
            DisplayDescription();
            DisplayLevel();
            DisplayBuildTime();
            DisplayBuildCost();

            if (!this.building.Buildable)
            {
                buttons.BuildButton.gameObject.SetActive(false);
            }
        }
    }
}