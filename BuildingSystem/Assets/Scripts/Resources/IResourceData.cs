﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Resources
{
    public interface IResourceData
    {
        string Name { get; }
        string Description { get; }
        Sprite Sprite { get; }
    }
}