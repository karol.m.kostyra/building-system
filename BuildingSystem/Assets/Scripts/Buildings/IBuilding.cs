﻿using System;
using Resources;

namespace Buildings
{
    public interface IBuilding
    {
        string Name { get; }
        string Description { get; }
        int CurrentLevel { get; set; }
        int MaxLevel { get; set; }
        float BuildTime { get; set; }
        IResourcesManager ResourcesManager { get; }
        event EventHandler<EventArgs> OnValueChanged;
    }
}