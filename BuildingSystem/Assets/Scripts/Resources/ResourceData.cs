﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Resources
{
    [CreateAssetMenu(fileName = "ResourceData", menuName = "Spaceship/Resources/ResourceData")]
    public class ResourceData : ScriptableObject, IResourceData
    {
        public string Name => this.name;
        public string Description => this.description;
        public Sprite Sprite => this.sprite;

        [SerializeField] protected new string name;
        [SerializeField] protected string description;
        [SerializeField] protected Sprite sprite;
    }
}