﻿using System;
using UnityEngine;

namespace Buildings
{
    public interface IBuildable
    {
        bool Buildable { get; set; }
        void Build();
    }
}