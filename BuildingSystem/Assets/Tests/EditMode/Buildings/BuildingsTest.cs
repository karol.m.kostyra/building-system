﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Buildings;
using NSubstitute;

namespace Tests.EditMode.Buildings
{
    public class BuildingsTest
    {
        [Test]
        public void CanUpgradeStorageBuilding()
        {
            //Arrange
            StorageBuilding storage = ScriptableObject.CreateInstance<StorageBuilding>();
            storage.CurrentLevel = 1;
            storage.MaxLevel = 3;
            
            //Act
            bool canUpgrade = storage.CanUpgrade();

            //Assert
            Assert.IsTrue(canUpgrade);
        }

        [Test]
        public void CannotUpgradeStorageWhenCurrentLevelEqualMaxLevel()
        {
            //Arrange
            StorageBuilding storage = ScriptableObject.CreateInstance<StorageBuilding>();
            storage.CurrentLevel = storage.MaxLevel = 3;
            
            //Act
            bool canUpgrade = storage.CanUpgrade();

            //Assert
            Assert.IsFalse(canUpgrade);
        }

        [Test]
        public void CannotUpgradeStorageWhenIsNotUpgradable()
        {
            //Arrange
            StorageBuilding storage = ScriptableObject.CreateInstance<StorageBuilding>();
            
            //Act
            bool canUpgrade = storage.CanUpgrade();

            //Assert
            Assert.IsFalse(canUpgrade);
        }
    }
}