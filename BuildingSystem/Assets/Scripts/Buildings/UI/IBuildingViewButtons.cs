﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Buildings
{
    public interface IBuildingViewButtons
    {
        Button BuildButton { get; }
        Button UpgradeButton { get; }
        void SetupBuildButton(DefaultBuildingType building);
        void SetupUpgradeButton(DefaultBuildingType building);
    }
}