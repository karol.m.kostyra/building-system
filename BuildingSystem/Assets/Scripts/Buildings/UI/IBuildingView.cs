﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Buildings
{
    public interface IBuildingView
    {
        IBuildingType Building { get; set; }
        IBuildingViewButtons BuildingViewButtons { get; }
        Text Name { get; }
        Text Description { get; }
        Text Level { get; }
        Text BuildTime { get; }
        GameObject BuildCostHandler { get; }
        GameObject BuildCostPrefab { get; }
        void Initialize();
        void DisplayName();
        void DisplayDescription();
        void DisplayLevel();
        void DisplayBuildTime();
        void DisplayBuildCost();
        void UpdateData(object sender, EventArgs e);
    }
}