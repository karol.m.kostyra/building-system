﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Buildings
{
    public interface IUpgradable
    {
        bool CanUpgrade();
        void Upgrade();
    }
}