﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Buildings
{
    public interface IStorageBuilding
    {
        int Capacity { get;}
        int CapacityPerLevel { get; }
    }
}