﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Resources
{
    [Serializable]
    public class Resource : IResource
    {
        public IResourceData ResourceData => this.resourceData;
        public int Amount => this.amount;

        [SerializeField] protected ResourceData resourceData;
        [SerializeField] protected int amount;
    }
}