﻿namespace Buildings
{
    public interface IBuildingController
    {
        IBuilding Building { get; }
    }
}