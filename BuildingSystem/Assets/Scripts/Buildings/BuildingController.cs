﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Buildings
{
    public class BuildingController : MonoBehaviour, IBuildingController
    {
        public IBuilding Building => this.building;

        [SerializeField] private Building building = null;
    }
}