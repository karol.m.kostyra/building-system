﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Buildings
{
    public interface IBuildCostPrefabManager
    {
        Text ResourceName { get; }
        Image ResourceImage { get; }
        void SetResourceName(string name);
        void SetResourceImage(Sprite sprite);
    }
}